package com.epam.task.compts;

public class DniproComponents implements Addable{
  private int dough;
  private int sauce;
  private int cheese;

  public DniproComponents() {
    dough = 9;
    sauce = 2;
    cheese = 1;
  }

  public int addDough() {
    return dough;
  }

  public int addSauce() {
    return sauce;
  }

  public int addCheese() {
    return cheese;
  }
}
