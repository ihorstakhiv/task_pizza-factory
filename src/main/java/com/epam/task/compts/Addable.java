package com.epam.task.compts;

public interface Addable {
  int addDough();
  int addSauce();
  int addCheese();
}
