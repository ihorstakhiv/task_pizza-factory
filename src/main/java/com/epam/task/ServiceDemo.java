package com.epam.task;

import com.epam.task.pizaa.PizzaType;
import com.epam.task.service.ServiceLviv;

public class ServiceDemo {
  
  public static void main(String[] args) {
    new ServiceLviv().createPizza(PizzaType.Pepperoni);
  }
}
