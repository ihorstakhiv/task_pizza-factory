package com.epam.task.pizaa;

import com.epam.task.compts.Addable;

public class VeggiePizza extends Pizza {

  public VeggiePizza(Addable st) {
    super(st);
  }

  public void prepare() {
    store.addDough();
    store.addSauce();
    store.addCheese();
  }
}
