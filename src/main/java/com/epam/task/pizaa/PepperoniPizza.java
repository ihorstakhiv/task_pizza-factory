package com.epam.task.pizaa;

import com.epam.task.compts.Addable;

public class PepperoniPizza extends Pizza{

  public PepperoniPizza(Addable st) {
    super(st);
  }

  public void prepare() {
    store.addDough();
    store.addSauce();
    store.addCheese();
  }
}
