package com.epam.task.pizaa;

public enum PizzaType {
  Pepperoni, Cheese, Clam, Veggie
}
