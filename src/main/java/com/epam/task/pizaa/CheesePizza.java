package com.epam.task.pizaa;

import com.epam.task.compts.Addable;

public class CheesePizza extends com.epam.task.pizaa.Pizza {

  public CheesePizza(Addable st) {
    super(st);
  }

  public void prepare() {
    store.addDough();
    store.addSauce();
    store.addCheese();
  }
}
