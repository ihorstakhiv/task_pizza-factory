package com.epam.task.pizaa;

import com.epam.task.compts.Addable;

public class ClamPizza extends Pizza {

  public ClamPizza(Addable st) {
    super(st);
  }

  public void prepare() {
    store.addDough();
    store.addSauce();
    store.addCheese();
  }
}
