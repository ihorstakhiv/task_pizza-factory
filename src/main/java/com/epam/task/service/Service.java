package com.epam.task.service;

import com.epam.task.pizaa.Pizza;
import com.epam.task.pizaa.PizzaType;

public abstract class Service {

  Pizza orderPizza(PizzaType what) {
    Pizza pizza = null;
    pizza = createPizza(what);
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    pizza.box();
    return pizza;
  }
  abstract Pizza createPizza(PizzaType type);
}
