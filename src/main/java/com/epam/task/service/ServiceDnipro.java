package com.epam.task.service;

import com.epam.task.compts.Addable;
import com.epam.task.compts.DniproComponents;
import com.epam.task.pizaa.CheesePizza;
import com.epam.task.pizaa.ClamPizza;
import com.epam.task.pizaa.PepperoniPizza;
import com.epam.task.pizaa.Pizza;
import com.epam.task.pizaa.PizzaType;
import com.epam.task.pizaa.VeggiePizza;

public class ServiceDnipro extends Service {

  public Pizza createPizza(PizzaType type) {
    Addable store = new DniproComponents();
    switch(type) {
      case Pepperoni:
        return new PepperoniPizza(store);
      case Clam:
        return new ClamPizza(store);
      case Cheese:
        return new CheesePizza(store);
      case Veggie:
        return new VeggiePizza(store);
      default:
        System.out.println("No such pizza in out menu");
        return null;
    }
  }
}
